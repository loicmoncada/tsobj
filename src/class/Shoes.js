"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.Shoes = void 0;
var Product_1 = require("./Product");
var Shoes = /** @class */ (function (_super) {
    __extends(Shoes, _super);
    //classe qui hérite de Product
    function Shoes(sSize, productId, name, weight, price, length, width, height) {
        var _this = _super.call(this, productId, name, weight, price, length, width, height) || this;
        _this.sSize = sSize;
        return _this;
    }
    Shoes.prototype.displayDetails = function () {
        return "productId:".concat(this.productId, ",name:").concat(this.name, ",weight:").concat(this.weight, ",\n        price:").concat(this.price, ",dim:[length:").concat(this.dim.length, ",width:").concat(this.dim.width, ",height:").concat(this.dim.height, "],ShoesSize:").concat(this.sSize);
    };
    return Shoes;
}(Product_1.Product));
exports.Shoes = Shoes;
