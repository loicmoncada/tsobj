"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StandardDelivery = void 0;
//on implement l'interface deliverable qui contient des methodes deja typé
var StandardDelivery = /** @class */ (function () {
    function StandardDelivery() {
    }
    StandardDelivery.prototype.estimateDeliveryTime = function (weight) {
        if (weight < 10) {
            return 7;
        }
        else {
            return 10;
        }
    };
    StandardDelivery.prototype.calculateShippingFee = function (weight) {
        if (weight < 1) {
            return 5;
        }
        else if (weight >= 1 && weight <= 5) {
            return 10;
        }
        else if (weight > 5) {
            return 20;
        }
        else {
            return 99;
        }
    };
    return StandardDelivery;
}());
exports.StandardDelivery = StandardDelivery;
