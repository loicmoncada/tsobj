import { Adress } from "../type/type";

export class Customer {
  customerId = 0;
  name = "";
  email = "";
  coord: Adress | undefined;

  constructor(customerId: number, name: string, email: string) {
    this.customerId = customerId;
    this.name = name;
    this.email = email;
  }
  displayInfo(): string {
    return `customerId:${this.customerId},name:${this.name},email:${this.email},coord:${this.coord}`;
  }
  displayAddress() {
    return this.coord;
  }
  setAddress(address: Adress){
    this.coord = address
  }
}
