import { Product } from "./Product";
import { ShoeSize } from "../type/enum";

export class Shoes extends Product {
  sSize: ShoeSize | undefined;
  //classe qui hérite de Product
  constructor(
    sSize: ShoeSize,
    productId: number,
    name: string,
    weight: number,
    price: number,
    length: number,
    width: number,
    height: number
  ) {
    super(productId, name, weight, price, length, width, height);
    this.sSize = sSize;
  }
  displayDetails(): string {
    return `productId:${this.productId},name:${this.name},weight:${this.weight},
        price:${this.price},dim:[length:${this.dim.length},width:${this.dim.width},height:${this.dim.height}],ShoesSize:${this.sSize}`;
  }
}
