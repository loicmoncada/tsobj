import { Dimensions } from "../type/type";

export class Product {
  productId = 0;
  name = "";
  weight = 0;
  price = 0;
  dim: Dimensions = { length: 0, width: 0, height: 0 };
  //ici je prefere récupéré mes dimensions une par une et non sous forme d'objet
  constructor(
    productId: number,
    name: string,
    weight: number,
    price: number,
    length: number,
    width: number,
    height: number
  ) {
    this.dim = { length: length, width: width, height: height };
    this.productId = productId;
    this.name = name;
    this.weight = weight;
    this.price = price;
  }
  displayDetails(): string {
    return `productId:${this.productId},name:${this.name},weight:${this.weight},
        price:${this.price},dim:[length:${this.dim.length},width:${this.dim.width},height:${this.dim.height}]`;
  }
}
