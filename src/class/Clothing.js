"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.Clothe = void 0;
var Product_1 = require("./Product");
var Clothe = /** @class */ (function (_super) {
    __extends(Clothe, _super);
    //classe qui hérite de Product
    function Clothe(cSize, productId, name, weight, price, length, width, height) {
        var _this = _super.call(this, productId, name, weight, price, length, width, height) || this;
        _this.cSize = cSize;
        return _this;
    }
    Clothe.prototype.displayDetails = function () {
        return "productId:".concat(this.productId, ",name:").concat(this.name, ",weight:").concat(this.weight, ",\n        price:").concat(this.price, ",dim:[length:").concat(this.dim.length, ",width:").concat(this.dim.width, ",height:").concat(this.dim.height, "],ClotheSize:").concat(this.cSize);
    };
    return Clothe;
}(Product_1.Product));
exports.Clothe = Clothe;
