"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExpressDelivery = void 0;
//on implement l'interface deliverable qui contient des methodes deja typé
var ExpressDelivery = /** @class */ (function () {
    function ExpressDelivery() {
    }
    ExpressDelivery.prototype.estimateDeliveryTime = function (weight) {
        if (weight <= 5) {
            return 1;
        }
        else {
            return 3;
        }
    };
    ExpressDelivery.prototype.calculateShippingFee = function (weight) {
        if (weight <= 5) {
            return 8;
        }
        else if (weight >= 1 && weight <= 5) {
            return 14;
        }
        else if (weight > 5) {
            return 30;
        }
        else {
            return 99;
        }
    };
    return ExpressDelivery;
}());
exports.ExpressDelivery = ExpressDelivery;
