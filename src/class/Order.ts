import { Product } from "./Product";
import { Customer } from "./Customer";
import { Deliverable } from "../type/Deliverable";
import { StandardDelivery } from "./StandardDelivery";

export class Order implements Deliverable {
  orderId: number | undefined;
  customer: Customer | undefined;
  //ici on a un array de product (important pour faire un pannier)
  productsList: Array<Product>;
  orderDate: Date | undefined;
  //on la déclare comme standardDelivery par défault
  delivery: StandardDelivery | undefined;

  constructor(
    orderId: number,
    customer: Customer | undefined,
    productsList: Array<Product>,
    orderDate: Date | undefined
  ) {
    this.orderId = orderId;
    this.customer = customer;
    this.productsList = productsList;
    this.orderDate = orderDate;
  }
  //c'est ici qu'on peut modifier le systeme de commande (standar ou express)
  setDelivery(type: any) {
    this.delivery = new type();
  }
  //on a les deux calc de frais et de temps de livraison
  calculateShippingFee() {
    return this.delivery.calculateShippingFee(this.calculateWeight());
  }

  estimateDeliveryTime() {
    return this.delivery.estimateDeliveryTime(this.calculateWeight());
  }
  //on peut ajouter un produit
  addProduct(product: Product) {
    this.productsList.push(product);
  }
  //on peut supp un produit
  removeProduct(productId: number) {
    this.productsList.forEach((e) => {
      if (e.productId == productId) {
        this.productsList.splice(this.productsList.indexOf(e), 1);
      }
    });
  }
  //foreach pour faire la somme de tous les poids
  calculateWeight() {
    let totalWeight = 0;
    this.productsList.forEach((e) => {
      totalWeight += e.weight;
    });
    return totalWeight;
  }

  //foreach pour faire la somme de tous les prix
  calculateTotal() {
    let totalPrice = 0;
    this.productsList.forEach((e) => {
      totalPrice += e.price;
    });
    return totalPrice;
  }
  //affiche la commande sous forme de string
  displayOrder() {
    let end = "";
    this.productsList.forEach((e) => {
      end += `un(e) ${e.name} à ${e.price}€ \n`;
    });
    return `
Commande éffectué par ${this.customer?.name} 
avec l'email : ${this.customer?.email}
        
voici ce qu'il a commandé:
${end}
        
ce qui coutera : ${this.calculateTotal()}€
        `;
  }
}
