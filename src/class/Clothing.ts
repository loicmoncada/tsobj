import { Product } from "./Product";
import { ClothingSize } from "../type/enum";

export class Clothe extends Product {
  cSize: ClothingSize | undefined;
  //classe qui hérite de Product
  constructor(
    cSize: ClothingSize,
    productId: number,
    name: string,
    weight: number,
    price: number,
    length: number,
    width: number,
    height: number
  ) {
    super(productId, name, weight, price, length, width, height);
    this.cSize = cSize;
  }
  displayDetails(): string {
    return `productId:${this.productId},name:${this.name},weight:${this.weight},
        price:${this.price},dim:[length:${this.dim.length},width:${this.dim.width},height:${this.dim.height}],ClotheSize:${this.cSize}`;
  }
}
