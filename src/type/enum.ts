export enum ClothingSize {
  XS,
  S,
  M,
  L,
  XL,
  XXL,
}
export enum ShoeSize {
  S_36,
  S_37,
  S_38,
  S_39,
  S_40,
  S_41,
  S_42,
  S_43,
  S_44,
  S_45,
  S_46,
}
