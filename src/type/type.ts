export type Dimensions = {
  length: number;
  width: number;
  height: number;
};
export type Adress = {
  street: string;
  city: string;
  postalCode: string;
  country: string;
};
