"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShoeSize = exports.ClothingSize = void 0;
var ClothingSize;
(function (ClothingSize) {
    ClothingSize[ClothingSize["XS"] = 0] = "XS";
    ClothingSize[ClothingSize["S"] = 1] = "S";
    ClothingSize[ClothingSize["M"] = 2] = "M";
    ClothingSize[ClothingSize["L"] = 3] = "L";
    ClothingSize[ClothingSize["XL"] = 4] = "XL";
    ClothingSize[ClothingSize["XXL"] = 5] = "XXL";
})(ClothingSize || (exports.ClothingSize = ClothingSize = {}));
var ShoeSize;
(function (ShoeSize) {
    ShoeSize[ShoeSize["s36"] = 0] = "s36";
    ShoeSize[ShoeSize["s37"] = 1] = "s37";
    ShoeSize[ShoeSize["s38"] = 2] = "s38";
    ShoeSize[ShoeSize["s39"] = 3] = "s39";
    ShoeSize[ShoeSize["s40"] = 4] = "s40";
    ShoeSize[ShoeSize["s41"] = 5] = "s41";
    ShoeSize[ShoeSize["s42"] = 6] = "s42";
    ShoeSize[ShoeSize["s43"] = 7] = "s43";
    ShoeSize[ShoeSize["s44"] = 8] = "s44";
    ShoeSize[ShoeSize["s45"] = 9] = "s45";
    ShoeSize[ShoeSize["s46"] = 10] = "s46";
})(ShoeSize || (exports.ShoeSize = ShoeSize = {}));
