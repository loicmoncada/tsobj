import { Clothe } from "./src/class/Clothing";
import { Customer } from "./src/class/Customer";
import { ExpressDelivery } from "./src/class/ExpressDelivery";
import { Order } from "./src/class/Order";
import { Product } from "./src/class/Product";
import { Shoes } from "./src/class/Shoes";
import { StandardDelivery } from "./src/class/StandardDelivery";
import { ClothingSize, ShoeSize } from "./src/type/enum";

//on creer des produits (1 chaussure, 1 vetement et un produit général)
let cookie = new Product(1, "cookie", 1, 0.1, 2, 3, 2);
//l'enum de Shoes est "s" suivit de la taille désiré de 36 à 46
let basket = new Shoes(ShoeSize.S_40, 2, "bask", 2, 59.99, 5, 5, 5);
//l'enum de clothe utilise les tailles XS, S, M, L, XL, XXL
let pull = new Clothe(ClothingSize.XL, 3, "pull", 50, 59.99, 5, 5, 5);
//on creer un client
let paul = new Customer(1, "paul", "paupaul@gmail.com");
paul.setAddress({street:"rue",city:"une ville",postalCode:"01000",country:"pays"})
console.log(paul.displayAddress())
//on creer la commande du client paul
let order1 = new Order(1, paul, [], new Date("1995-12-17T03:24:00"));
//on ajoute trois produits
order1.addProduct(cookie);
order1.addProduct(basket);
order1.addProduct(pull);
//on affiche la commande
console.log(order1.displayOrder());
//on supprime le pull
order1.removeProduct(3);
//on verifie que le pull n'y est plus
console.log(order1.displayOrder());
//on dit que la commande est en standard (meme s'il elle est de base en standard)
order1.setDelivery(StandardDelivery);
//on affiche les calc des frais et de l'estimation du temps de livraison
console.log("Livraison Standard: ");
console.log(order1.estimateDeliveryTime());
console.log(order1.calculateShippingFee());
//on passe en commande express
order1.setDelivery(ExpressDelivery);
//on verifie que la modification fonctionne
console.log("\nLivraison Express: ");
console.log(order1.estimateDeliveryTime());
console.log(order1.calculateShippingFee());
