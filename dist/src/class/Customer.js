"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Customer = void 0;
var Customer = /** @class */ (function () {
    function Customer(customerId, name, email) {
        this.customerId = 0;
        this.name = "";
        this.email = "";
        this.customerId = customerId;
        this.name = name;
        this.email = email;
    }
    Customer.prototype.displayInfo = function () {
        return "customerId:".concat(this.customerId, ",name:").concat(this.name, ",email:").concat(this.email, ",coord:").concat(this.coord);
    };
    Customer.prototype.displayAddress = function () {
        return this.coord;
    };
    return Customer;
}());
exports.Customer = Customer;
