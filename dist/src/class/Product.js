"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Product = void 0;
var Product = /** @class */ (function () {
    //ici je prefere récupéré mes dimensions une par une et non sous forme d'objet
    function Product(productId, name, weight, price, length, width, height) {
        this.productId = 0;
        this.name = "";
        this.weight = 0;
        this.price = 0;
        this.dim = { length: 0, width: 0, height: 0 };
        this.dim = { length: length, width: width, height: height };
        this.productId = productId;
        this.name = name;
        this.weight = weight;
        this.price = price;
    }
    Product.prototype.displayDetails = function () {
        return "productId:".concat(this.productId, ",name:").concat(this.name, ",weight:").concat(this.weight, ",\n        price:").concat(this.price, ",dim:[length:").concat(this.dim.length, ",width:").concat(this.dim.width, ",height:").concat(this.dim.height, "]");
    };
    return Product;
}());
exports.Product = Product;
