"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Order = void 0;
var Order = /** @class */ (function () {
    function Order(orderId, customer, productsList, orderDate) {
        this.orderId = orderId;
        this.customer = customer;
        this.productsList = productsList;
        this.orderDate = orderDate;
    }
    //c'est ici qu'on peut modifier le systeme de commande (standar ou express)
    Order.prototype.setDelivery = function (type) {
        this.delivery = new type;
    };
    //on a les deux calc de frais et de temps de livraison
    Order.prototype.calculateShippingFee = function () {
        return this.delivery.calculateShippingFee(this.calculateWeight());
    };
    Order.prototype.estimateDeliveryTime = function () {
        return this.delivery.estimateDeliveryTime(this.calculateWeight());
    };
    //on peut ajouter un produit
    Order.prototype.addProduct = function (product) {
        this.productsList.push(product);
    };
    //on peut supp un produit
    Order.prototype.removeProduct = function (productId) {
        var _this = this;
        this.productsList.forEach(function (e) {
            if (e.productId == productId) {
                _this.productsList.splice(_this.productsList.indexOf(e), 1);
            }
        });
    };
    //foreach pour faire la somme de tous les poids
    Order.prototype.calculateWeight = function () {
        var totalWeight = 0;
        this.productsList.forEach(function (e) {
            totalWeight += e.weight;
        });
        return totalWeight;
    };
    //foreach pour faire la somme de tous les prix
    Order.prototype.calculateTotal = function () {
        var totalPrice = 0;
        this.productsList.forEach(function (e) {
            totalPrice += e.price;
        });
        return totalPrice;
    };
    //affiche la commande sous forme de string
    Order.prototype.displayOrder = function () {
        var _a, _b;
        var end = "";
        this.productsList.forEach(function (e) {
            end += "un(e) ".concat(e.name, " \u00E0 ").concat(e.price, "\u20AC \n");
        });
        return "\nCommande \u00E9ffectu\u00E9 par ".concat((_a = this.customer) === null || _a === void 0 ? void 0 : _a.name, " \navec l'email : ").concat((_b = this.customer) === null || _b === void 0 ? void 0 : _b.email, "\n        \nvoici ce qu'il a command\u00E9:\n").concat(end, "\n        \nce qui coutera : ").concat(this.calculateTotal(), "\u20AC\n        ");
    };
    return Order;
}());
exports.Order = Order;
