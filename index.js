"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Clothing_1 = require("./src/class/Clothing");
var Customer_1 = require("./src/class/Customer");
var ExpressDelivery_1 = require("./src/class/ExpressDelivery");
var Order_1 = require("./src/class/Order");
var Product_1 = require("./src/class/Product");
var Shoes_1 = require("./src/class/Shoes");
var StandardDelivery_1 = require("./src/class/StandardDelivery");
var enum_1 = require("./src/type/enum");
//on creer des produits (1 chaussure, 1 vetement et un produit général)
var cookie = new Product_1.Product(1, "cookie", 1, 0.1, 2, 3, 2);
//l'enum de Shoes est "s" suivit de la taille désiré de 36 à 46
var basket = new Shoes_1.Shoes(enum_1.ShoeSize.s40, 2, "bask", 2, 59.99, 5, 5, 5);
//l'enum de clothe utilise les tailles XS, S, M, L, XL, XXL
var pull = new Clothing_1.Clothe(enum_1.ClothingSize.XL, 3, "pull", 50, 59.99, 5, 5, 5);
//on creer un client
var paul = new Customer_1.Customer(1, "paul", "paupaul@gmail.com");
paul.setAddress({ street: "rue", city: "une ville", postalCode: "01000", country: "pays" });
console.log(paul.displayAddress());
//on creer la commande du client paul
var order1 = new Order_1.Order(1, paul, [], new Date("1995-12-17T03:24:00"));
//on ajoute trois produits
order1.addProduct(cookie);
order1.addProduct(basket);
order1.addProduct(pull);
//on affiche la commande
console.log(order1.displayOrder());
//on supprime le pull
order1.removeProduct(3);
//on verifie que le pull n'y est plus
console.log(order1.displayOrder());
//on dit que la commande est en standard (meme s'il elle est de base en standard)
order1.setDelivery(StandardDelivery_1.StandardDelivery);
//on affiche les calc des frais et de l'estimation du temps de livraison
console.log("Livraison Standard: ");
console.log(order1.estimateDeliveryTime());
console.log(order1.calculateShippingFee());
//on passe en commande express
order1.setDelivery(ExpressDelivery_1.ExpressDelivery);
//on verifie que la modification fonctionne
console.log("\nLivraison Express: ");
console.log(order1.estimateDeliveryTime());
console.log(order1.calculateShippingFee());
